using UnderTest.FeatureTransform.WorkBooks;

namespace UnderTest.FeatureTransform.Sheets
{
  public static class FeatureSheetStringExtensions
  {
    public static string MakeNameWorkSheetAcceptable(this string value)
    {
      return value.Truncate(30).RemoveBadCharsForWorksheetName();
    }
  }
}
