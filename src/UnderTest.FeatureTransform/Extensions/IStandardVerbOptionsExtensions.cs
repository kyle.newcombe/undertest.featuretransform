namespace UnderTest.FeatureTransform.Options
{
  public static class IStandardVerbOptionsExtensions
  {
    public static void DefaultWorkingDirectory(this IStandardVerbOptions instance, string currentDirectoryP)
    {
      if (string.IsNullOrWhiteSpace(instance.WorkingDirectory))
      {
        instance.WorkingDirectory = currentDirectoryP;
      }
    }
  }
}
