namespace UnderTest.FeatureTransform.Options
{
  using System.Collections.Generic;

  public interface IStandardVerbOptions
  {
    IEnumerable<string> FeaturePaths { get; set; }

    string OutputFilename { get; set; }

    string WorkingDirectory { get; set; }
  }
}
