using ClosedXML.Excel;
using System.Collections.Generic;
using System.Linq;

namespace UnderTest.FeatureTransform.Sheets
{
  public class TofSheet
  {
    private readonly XLWorkbook _workbook;
    private const string Name = "Table of Features";

    public TofSheet(XLWorkbook wbP)
    {
      _workbook = wbP;
      CreateTableOfFeaturesSheet();
    }

    public void CreateFeatureEntry(string featureNameP, IEnumerable<IXLCell> featureResultCellsP)
    {
      var featureSheet = featureNameP.MakeNameWorkSheetAcceptable();

      var resultCellsP = featureResultCellsP as IXLCell[] ?? featureResultCellsP.ToArray();
      var checkPassFormula = resultCellsP.Select(a => $"ISNUMBER(SEARCH(\"Pass\",'{featureSheet}'!A{a.Address.RowNumber.ToString()}))").ToList().Aggregate((a, b) => a + "," + b);
      var checkFailFormula = resultCellsP.Select(a => $"ISNUMBER(SEARCH(\"Fail\",'{featureSheet}'!A{a.Address.RowNumber.ToString()}))").ToList().Aggregate((a, b) => a + "," + b);

      var newRow = 1;
      var tofSheet = _workbook.Worksheet(Name);
      var lastRowUsed = tofSheet.LastRowUsed();
      if (lastRowUsed != null)
      {
        newRow = tofSheet.LastRowUsed().RowNumber() + 1;
      }

      const int currentCol = 1;
      var summaryCell = tofSheet.Cell(newRow, currentCol + 1);

      tofSheet.Cell(newRow, currentCol).Value = featureNameP;
      tofSheet.Cell(newRow, currentCol).Hyperlink = new XLHyperlink(featureSheet + "!A1");
      summaryCell.FormulaA1 =
        $"=IF(AND({checkPassFormula}),\"Pass\",IF(OR({checkFailFormula}),\"Fail\",\"Incomplete\"))";
      summaryCell.Style.Fill.BackgroundColor = XLColor.LightGray;
      summaryCell.AddConditionalFormat().WhenEquals("Pass").Fill.BackgroundColor = XLColor.Green;
      summaryCell.AddConditionalFormat().WhenEquals("Fail").Fill.BackgroundColor = XLColor.Red;
      summaryCell.AddConditionalFormat().WhenEquals("Incomplete").Fill.BackgroundColor = XLColor.Yellow;
    }

    private void CreateTableOfFeaturesSheet()
    {
      _workbook.Worksheets.Add(Name);
    }


    public void SetPosition(int positionP)
    {
      _workbook.Worksheets.Worksheet(Name).Position = positionP;
    }

    public void CreateInvalidFeatureEntry(string invalidFeatureNameP)
    {
      var newRow = 1;
      var tofSheet = _workbook.Worksheet(Name);
      var lastRowUsed = tofSheet.LastRowUsed();
      if (lastRowUsed != null)
      {
        newRow = tofSheet.LastRowUsed().RowNumber() + 1;
      }

      const int currentCol = 1;

      tofSheet.Cell(newRow, currentCol).Value = "Invalid Feature: "+invalidFeatureNameP;
      tofSheet.Cell(newRow, currentCol).Style.Font.FontColor = XLColor.Red;
    }
  }
}
