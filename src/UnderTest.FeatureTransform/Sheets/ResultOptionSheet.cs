using ClosedXML.Excel;
using System.Collections.Generic;

namespace UnderTest.FeatureTransform.Sheets
{
  public class ResultOptionSheet
  {
    private readonly XLWorkbook _workbook;
    private const string Name = "Result Options";

    private readonly List<string> _resultOptions;


    public ResultOptionSheet(XLWorkbook workBookP, List<string> resultOptionsP)
    {
      _workbook = workBookP;
      _resultOptions = resultOptionsP;
      CreateResultOptionSheet();
    }

    private void CreateResultOptionSheet()
    {
      var resultOptionSheet = _workbook.Worksheets.Add(Name);

      for (int i = 0; i < _resultOptions.Count; i++)
      {
        resultOptionSheet.Cell($"A{i + 1}").Value = _resultOptions[i];
      }

      resultOptionSheet.Cell($"A{_resultOptions.Count + 2}").Value = " ";
    }

    public void SetPosition(int positionP)
    {
      _workbook.Worksheets.Worksheet("Result Options").Position = positionP;
    }
  }
}
