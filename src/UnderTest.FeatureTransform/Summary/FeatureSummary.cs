using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Gherkin.Ast;
using UnderTest.FeatureTransform.Options;

namespace UnderTest.FeatureTransform.Summary
{
  public class FeatureSummary
  {
    public FeatureSummary(IStandardVerbOptions options, IEnumerable<string> files)
    {
      _options = options;
      _files = files;
    }

    private const string Title = "# Feature Summary";
    private const string TocTitle = "## TOC";

    private const string SummaryHeader = "| Feature | Description | Scenarios |";
    private const string SummaryDivider = "| --- | --- | --- |";

    private readonly StringBuilder _summaryContent = new StringBuilder();
    private string _rootDirName;
    private readonly IStandardVerbOptions _options;
    private readonly IEnumerable<string> _files;
    private void AddTocDirectoryList(IEnumerable<string> directories)
    {
      foreach (var directory in directories)
      {
        _summaryContent.AppendLine(directory == ""
          ? $"* [{_rootDirName}](#{_rootDirName.MakeHeaderLink()})"
          : $"* [{directory}](#{directory.MakeHeaderLink()})");
      }
    }

    private void AddDirectoryTitleAndTableHeaders(string directoryP)
    {
      string displayDirectoryName;
      if (directoryP == "")
      {
        displayDirectoryName = _rootDirName;
      }
      else
      {
        displayDirectoryName = directoryP.Replace(_options.WorkingDirectory, string.Empty) + "\\";
      }
      _summaryContent.AppendLine($"## {displayDirectoryName}");
      _summaryContent.AppendLine(SummaryHeader);
      _summaryContent.AppendLine(SummaryDivider);
    }

    private void AddFeaturesToDirectoryTable(IEnumerable<string> directoryFeaturesP)
    {
      foreach (var feature in directoryFeaturesP)
      {
        string featureName;
        var description = string.Empty;
        var scenarioList = string.Empty;

        try
        {
          var gherkinDocument = new Gherkin.Parser().Parse(Path.Combine(_options.WorkingDirectory, feature));
          featureName = gherkinDocument.Feature.Name;
          description = gherkinDocument.Feature.Description;
          scenarioList = GenerateScenarioList(gherkinDocument);
          if (!string.IsNullOrWhiteSpace(description))
          {
            description = gherkinDocument.Feature.Description
              .Replace(Environment.NewLine, "<br />");
          }
        }
        catch
        {
          featureName = "Invalid file";
        }

        if (string.IsNullOrWhiteSpace(featureName))
        {
          featureName = "(empty feature name)";
        }
        if (string.IsNullOrWhiteSpace(description))
        {
          description = "`no description`";
        }

        _summaryContent.AppendLine($"| [{featureName}](.\\{Uri.EscapeUriString(feature).UseBackslashForDirectories()}) | {description} | {scenarioList} |");
      }
    }

    private static string GenerateScenarioList(GherkinDocument gherkinDocumentP)
    {
      var listOfScenarios = string.Empty;
      foreach (var child in gherkinDocumentP.Feature.Children)
      {
        if (child is Scenario scenario)
        {
          listOfScenarios += $"<li>{scenario.Name}</li>";
        }
      }
      return $"<details><summary>Scenarios</summary><ul>{listOfScenarios}</ul></details>";
    }

    private void AddEachDirectoryFeatureSummaryTable(IEnumerable<string> directoriesP)
    {
      foreach (var directory in directoriesP)
      {
        AddDirectoryTitleAndTableHeaders(directory);

        var directoryFeatures = _files.Where(x => Path.GetDirectoryName(x) == directory).ToList();
        AddFeaturesToDirectoryTable(directoryFeatures);

        _summaryContent.AppendLine();
      }
    }

    public void CreateFeatureSummary()
    {
      _rootDirName = new DirectoryInfo(_options.WorkingDirectory).Name;

      var featureDirectories = _files.Where(x => !string.IsNullOrEmpty(x))
                                    .Select(Path.GetDirectoryName)
                                    .Distinct();

      _summaryContent.AppendLine(Title);
      _summaryContent.AppendLine(TocTitle);

      var directoriesP = featureDirectories as string[] ?? featureDirectories.ToArray();
      AddTocDirectoryList(directoriesP);
      AddEachDirectoryFeatureSummaryTable(directoriesP);

      File.WriteAllText(Path.Combine(_options.WorkingDirectory, _options.OutputFilename), _summaryContent.ToString());
    }
  }
}
