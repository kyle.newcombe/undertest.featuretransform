using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureTransform.Summary;
using UnderTest.FeatureTransform.WorkBooks;

namespace UnderTest.FeatureTransform.UnitTests.Extensions
{
  public class StringExtensionTests
  {
    [Test]
    public void Truncate_WhenPassedEmpty_ReturnsEmpty()
    {
      var input = string.Empty;

      var result = input.Truncate(10);

      result.Should().Be(string.Empty);
    }

    [Test]
    public void Truncate_PassedStringLongerThanMax_IsTruncated()
    {
      const string input = "This is something we need to truncate";

      var result = input.Truncate(10);

      result.Should().Be("This is so");
    }

    [TestCase("a/", "a_")]
    [TestCase(@"a\", "a_")]
    [TestCase("a's", "as")]
    public void RemoveBadCharsForWorksheetName_WhenPassedBadCharacters_RemovesThem(string badStringP, string goodStringP)
    {
      var result = badStringP.RemoveBadCharsForWorksheetName();
      result.Should().Be(goodStringP);
    }

    [TestCase("There are spaces in this header", "there-are-spaces-in-this-header")]
    [TestCase("There is a decimal 6.6 here and a ! in it", "there-is-a-decimal-6-6-here-and-a-in-it")]
    [TestCase("There is a weird character at the end of this header [prob a square bracket]", "there-is-a-weird-character-at-the-end-of-this-header-prob-a-square-bracket")]
    public void GetBackGoodHeaderLInk_WhenMakeHeaderLink_CreatesOne(string oldHeaderP, string goodHeaderLinkP)
    {
      var result = oldHeaderP.MakeHeaderLink();
      result.Should().Be(goodHeaderLinkP);
    }
  }
}
