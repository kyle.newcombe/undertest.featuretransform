Feature: Add to inventory after refund

If an item is returned, the item should be added to our inventory.